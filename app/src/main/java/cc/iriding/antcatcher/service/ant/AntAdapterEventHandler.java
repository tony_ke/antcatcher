package cc.iriding.antcatcher.service.ant;

import com.dsi.ant.channel.BackgroundScanState;
import com.dsi.ant.channel.BurstState;
import com.dsi.ant.channel.EventBufferSettings;
import com.dsi.ant.channel.IAntAdapterEventHandler;
import com.dsi.ant.message.LibConfig;

/**
 * Created by bac on 15/12/15.
 */
public class AntAdapterEventHandler implements IAntAdapterEventHandler {

    private AntService mService;
    AntAdapterEventHandler(AntService service){
        this.mService=service;
    }

    @Override
    public void onEventBufferSettingsChange(EventBufferSettings eventBufferSettings) {

    }

    @Override
    public void onBurstStateChange(BurstState burstState) {

    }

    @Override
    public void onLibConfigChange(LibConfig libConfig) {

    }

    @Override
    public void onBackgroundScanStateChange(BackgroundScanState backgroundScanState) {

    }
}
