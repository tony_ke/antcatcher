package cc.iriding.antcatcher.service.antplus;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.dsi.ant.plugins.antplus.pcc.AntPlusBikePowerPcc;
import com.dsi.ant.plugins.antplus.pcc.defines.DeviceState;
import com.dsi.ant.plugins.antplus.pcc.defines.EventFlag;
import com.dsi.ant.plugins.antplus.pcc.defines.RequestAccessResult;
import com.dsi.ant.plugins.antplus.pccbase.AntPluginPcc;
import com.dsi.ant.plugins.antplus.pccbase.PccReleaseHandle;

import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.util.EnumSet;

public class AntPlusService extends Service {

    private Logger log = Logger.getLogger(AntPlusService.class);

    private PccReleaseHandle<AntPlusBikePowerPcc> mBikePowerHandle;
    private AntPlusBikePowerPcc mBikePowerPcc;

    IAntPlusServiceListener mListener = new IAntPlusServiceListener.BaseAntPlusServiceListener();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        log.warn("AntPlusService Destroy");
        stopBikePower();

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new AntPlusServiceBinder(this);
    }

    void startBikePower(final int deviceNumber, final int wheelCircumference) {
        if (mBikePowerHandle != null) {
            stopBikePower();
        }
        log.info("请求读取BikePower数据,deviceNumber="+deviceNumber);
        mBikePowerHandle = AntPlusBikePowerPcc.requestAccess(this, deviceNumber, 0, new AntPluginPcc.IPluginAccessResultReceiver<AntPlusBikePowerPcc>() {
            @Override
            public void onResultReceived(AntPlusBikePowerPcc pcc, RequestAccessResult result, DeviceState state) {
                log.debug("onResultReceived,result=" + result + ",deviceState=" + state);

                mBikePowerPcc=pcc;

                if (result == RequestAccessResult.SUCCESS) {
                    final BikePowerData data = new BikePowerData();
                    pcc.subscribeCalculatedPowerEvent(new AntPlusBikePowerPcc.ICalculatedPowerReceiver() {
                        @Override
                        public void onNewCalculatedPower(long l, EnumSet<EventFlag> enumSet, AntPlusBikePowerPcc.DataSource dataSource, BigDecimal bigDecimal) {
                            data.power = bigDecimal.floatValue();
                        }
                    });
                    pcc.subscribeCalculatedWheelSpeedEvent(new AntPlusBikePowerPcc.CalculatedWheelSpeedReceiver(new BigDecimal(wheelCircumference/1000f)) {
                        @Override
                        public void onNewCalculatedWheelSpeed(long l, EnumSet<EventFlag> enumSet, AntPlusBikePowerPcc.DataSource dataSource, BigDecimal bigDecimal) {
                            data.speed = bigDecimal.floatValue();
                            mListener.onReceiveBikePowerData(data);
                        }
                    });
                } else {
                    stopBikePower();
                    if (result!=RequestAccessResult.USER_CANCELLED){
                        startBikePower(deviceNumber,wheelCircumference);
                    }else{
                        log.error("请求失败了:" + result);
                    }
                }
            }
        }, new AntPluginPcc.IDeviceStateChangeReceiver() {
            @Override
            public void onDeviceStateChange(DeviceState deviceState) {
                log.info("onDeviceStateChange:" + deviceState);
                if (deviceState==DeviceState.DEAD || deviceState == DeviceState.CLOSED){
                    stopBikePower();
                    startBikePower(deviceNumber,wheelCircumference);
                }
            }
        });
    }

    void stopBikePower() {
        if (mBikePowerPcc!=null){
            log.debug("释放BikePowerPcc");
            mBikePowerPcc.releaseAccess();
        }
        if (mBikePowerHandle != null) {
            log.debug("关闭BikePowerHandle");
            mBikePowerHandle.close();
            mBikePowerHandle=null;
        }
    }

    public class BikePowerData {
        private float speed;
        private float power;

        public float getSpeed() {
            return speed;
        }

        public float getPower() {
            return power;
        }
    }
}
