package cc.iriding.antcatcher;

import android.app.Dialog;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dsi.ant.message.fromant.DataMessage;
import com.dsi.ant.message.fromant.MessageFromAntType;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

import cc.iriding.antcatcher.service.ant.AntService;
import cc.iriding.antcatcher.service.ant.AntServiceBinder;
import cc.iriding.antcatcher.service.ant.IAntServiceListener;
import cc.iriding.antcatcher.service.antplus.AntPlusService;
import cc.iriding.antcatcher.service.antplus.IAntPlusServiceListener;
import cc.iriding.antcatcher.utils.SPUtils;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {
    private Logger log = Logger.getLogger(MainActivity.class);

    private final int[] mGearsFront = new int[]{50, 34};
    private final int[] mGearsRear = new int[]{11, 12, 13, 14, 15, 17, 19, 21, 23, 25, 28};
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final int wheelCircumference = 2146;

    private Handler mHandler = new Handler();

    @ViewById
    TextView txtDi2Battery;
    @ViewById
    TextView txtFront;
    @ViewById
    TextView txtRear;
    @ViewById
    TextView txtRatio;
    @ViewById
    TextView txtPower;
    @ViewById
    TextView txtSpeed;
    @ViewById
    TextView txtCadence;
    @ViewById
    TextView txtLastUpdatetime;

    private Dialog mDialogSelectDevice;

    private AntServiceBinder mAntService;
//    private AntPlusServiceBinder mAntPlusService;

    private Date lastDi2UpdateTime;
    private Date lastPowerDeviceUpdateTime;
    private float mCurrentGearRatio;//当前的变速齿比

    private int mDi2DeviceNumber=0;
    private int mBikePowerDeviceNumber=3;


    @AfterViews
    void onCreated() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (SPUtils.getInt(this, "di2_device_number", -1) == -1) {
            SPUtils.saveInt(this, "di2_device_number", mDi2DeviceNumber);
        }else{
            mDi2DeviceNumber=SPUtils.getInt(this, "di2_device_number", mDi2DeviceNumber);
        }
        if (SPUtils.getInt(this, "bikepower_device_number", -1) == -1) {
            SPUtils.saveInt(this, "bikepower_device_number", mBikePowerDeviceNumber);
        }else{
            mBikePowerDeviceNumber=SPUtils.getInt(this, "bikepower_device_number", mBikePowerDeviceNumber);
        }

        if(!bindService(new Intent(this, AntService.class), mAntServiceConnection, Service.BIND_AUTO_CREATE)){
            log.error("绑定AntService失败了");
        }
//        if(!bindService(new Intent(this, AntPlusService.class), mAntPlusServiceConnection, BIND_AUTO_CREATE)){
//            log.error("绑定AntPlusService失败了");
//        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onDestroy() {

        unbindService(mAntServiceConnection);
//        unbindService(mAntPlusServiceConnection);

        super.onDestroy();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.setting_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_settings) {
            mDialogSelectDevice=new AlertDialog.Builder(this).setView(getLayoutInflater().inflate(R.layout.dialog_select_device,null)).create();
            mDialogSelectDevice.setCancelable(true);

            mDialogSelectDevice.show();

            final EditText txtDi2= (EditText) mDialogSelectDevice.findViewById(R.id.txtDi2);
            final EditText txtBikePower= (EditText) mDialogSelectDevice.findViewById(R.id.txtBikePower);
            txtDi2.setText(SPUtils.getInt(this,"di2_device_number",0)+"");
            txtBikePower.setText(SPUtils.getInt(this,"bikepower_device_number",0)+"");
            mDialogSelectDevice.findViewById(R.id.btnSure).setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    SPUtils.saveInt(MainActivity.this,"di2_device_number",Integer.parseInt(txtDi2.getText().toString()));
                    SPUtils.saveInt(MainActivity.this,"bikepower_device_number",Integer.parseInt(txtBikePower.getText().toString()));

                    MainActivity.this.finish();
                    startActivity(new Intent(MainActivity.this,MainActivity_.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }

    void onAntReceiveMessage(MessageFromAntType type, DataMessage message) {
        byte[] payload = message.getPayload();
        if (payload[0] == 0x00) {

            lastDi2UpdateTime = new Date();
            updateTime();

            int front = payload[2];
            int rear = payload[3];
            int battery = payload[4];
            log.info("front:" + front + " rear:" + rear + " battery:" + battery);
            txtDi2Battery.setText("" + battery + "%");
            if (front > 0) {
                txtFront.setText("" + front);
            }
            if (rear > 0) {
                txtRear.setText("" + rear);
            }

            if (front > 0 && rear > 0) {
                mCurrentGearRatio = (float) (mGearsFront[mGearsFront.length - front])
                        / (float) mGearsRear[mGearsRear.length - rear];

                txtRatio.setText("" + mGearsFront[mGearsFront.length - front]
                        + ":" + mGearsRear[mGearsRear.length - rear]);
            }
        }
    }

    private void onAntPlusReceviceBikePowerData(AntPlusService.BikePowerData data) {
        lastPowerDeviceUpdateTime = new Date();
        updateTime();
        float power = data.getPower();
        txtPower.setText(power + "");
        txtSpeed.setText(data.getSpeed() + "");

        log.info("power:" + power + " speed:" + data.getSpeed() + " ratio:" + mCurrentGearRatio);
        long cadence = 0;
        if (power > 0) {
            cadence = Math.round((data.getSpeed() / 3.6 * 60) / (wheelCircumference / 1000) / mCurrentGearRatio);
        }
        txtCadence.setText(cadence + "");
        log.info("cadence:" + cadence);

    }

    private void updateTime() {
        StringBuilder sb = new StringBuilder("变速:");
        if (lastDi2UpdateTime != null) {
            sb.append(sdf.format(lastDi2UpdateTime));
        } else {
            sb.append("无");
        }
        sb.append("\n功率:");
        if (lastPowerDeviceUpdateTime != null) {
            sb.append(sdf.format(lastPowerDeviceUpdateTime));
        } else {
            sb.append("无");
        }
        txtLastUpdatetime.setText(sb.toString());
    }

    private IAntServiceListener mAntServiceListener = new IAntServiceListener() {
        @Override
        public void onReceiveMessage(final MessageFromAntType type, final DataMessage message) {

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    onAntReceiveMessage(type, message);
                }
            });
        }

        @Override
        public void onDisconnected() {

        }
    };

    private IAntPlusServiceListener mAntPlusServiceListener = new IAntPlusServiceListener() {
        @Override
        public void onDisconnected() {

        }

        @Override
        public void onReceiveBikePowerData(final AntPlusService.BikePowerData data) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    onAntPlusReceviceBikePowerData(data);
                }
            });
        }
    };

    private ServiceConnection mAntServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mAntService = (AntServiceBinder) iBinder;
            mAntService.setListener(mAntServiceListener);
            mAntService.start(mDi2DeviceNumber);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            log.warn("ant service disconnected");
        }
    };
//    private ServiceConnection mAntPlusServiceConnection = new ServiceConnection() {
//        @Override
//        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
//            mAntPlusService = (AntPlusServiceBinder) iBinder;
//            mAntPlusService.setListener(mAntPlusServiceListener);
//            mAntPlusService.startBikePower(mBikePowerDeviceNumber, wheelCircumference);
//        }
//
//        @Override
//        public void onServiceDisconnected(ComponentName componentName) {
//            log.warn("ant plus service disconnected");
//        }
//    };

    private long mLastBackClickTime = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            long now = new Date().getTime();
            if (now - mLastBackClickTime < 2000) {
                return super.onKeyDown(keyCode, event);
            }
            Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
            mLastBackClickTime = now;
            return false;
        } else {
            return true;
        }
    }
}
