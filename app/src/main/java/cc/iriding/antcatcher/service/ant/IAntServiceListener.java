package cc.iriding.antcatcher.service.ant;

import com.dsi.ant.message.fromant.DataMessage;
import com.dsi.ant.message.fromant.MessageFromAntType;

/**
 * Created by bac on 15/12/15.
 */
public interface IAntServiceListener {
    void onReceiveMessage(MessageFromAntType type, DataMessage message);
    void onDisconnected();

    public static class BaseAntChannelServiceListener implements IAntServiceListener {
        @Override
        public void onReceiveMessage(MessageFromAntType type, DataMessage message) {
        }

        @Override
        public void onDisconnected() {
        }
    }
}
