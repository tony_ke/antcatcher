package cc.iriding.antcatcher.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by bac on 15/12/15.
 */
public class SPUtils {

    private static final String SharedPreferencesDefaultName = "settings";

    //整数
    public static boolean saveInt(Context cxt,String name, int value) {
        SharedPreferences sp = cxt.getSharedPreferences(SharedPreferencesDefaultName, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.putInt(name, value);
        return edit.commit();
    }

    public static int getInt(Context cxt,String name, int value) {
        SharedPreferences sp = cxt.getSharedPreferences(SharedPreferencesDefaultName, Context.MODE_PRIVATE);
        return sp.getInt(name, value);
    }
}
