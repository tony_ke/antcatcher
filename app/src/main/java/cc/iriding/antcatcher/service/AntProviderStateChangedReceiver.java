package cc.iriding.antcatcher.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.dsi.ant.channel.AntChannelProvider;

import cc.iriding.antcatcher.service.ant.AntService;

/**
 * Created by bac on 15/12/15.
 */
public class AntProviderStateChangedReceiver extends BroadcastReceiver {

    private AntService mService;

    public AntProviderStateChangedReceiver(AntService service){
        this.mService=service;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (AntChannelProvider.ACTION_CHANNEL_PROVIDER_STATE_CHANGED.equals(intent.getAction())) {
            int numChannels = intent.getIntExtra(AntChannelProvider.NUM_CHANNELS_AVAILABLE, 0);
            boolean legacyInterfaceInUse = intent.getBooleanExtra(AntChannelProvider.LEGACY_INTERFACE_IN_USE,
                    false);
            mService.onAntProviderStateChanged(numChannels,legacyInterfaceInUse);
        }
    }
}
