package cc.iriding.antcatcher.service.antplus;

/**
 * Created by bac on 15/12/15.
 */
public interface IAntPlusServiceListener {

    void onDisconnected();

    void onReceiveBikePowerData(AntPlusService.BikePowerData data);

    public class BaseAntPlusServiceListener implements IAntPlusServiceListener{

        @Override
        public void onDisconnected() {
        }

        @Override
        public void onReceiveBikePowerData(AntPlusService.BikePowerData data) {
        }
    }
}
