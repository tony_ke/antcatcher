package cc.iriding.antcatcher.service.ant;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.RemoteException;

import com.dsi.ant.channel.AntChannel;
import com.dsi.ant.channel.AntChannelProvider;
import com.dsi.ant.channel.AntCommandFailedException;
import com.dsi.ant.channel.ChannelNotAvailableException;
import com.dsi.ant.channel.NetworkKey;
import com.dsi.ant.channel.UnsupportedFeatureException;
import com.dsi.ant.message.ChannelId;
import com.dsi.ant.message.ChannelType;
import com.dsi.ant.message.fromant.ChannelEventMessage;
import com.dsi.ant.message.fromant.DataMessage;
import com.dsi.ant.message.fromant.MessageFromAntType;

import org.apache.log4j.Logger;

import cc.iriding.antcatcher.service.AntProviderStateChangedReceiver;
import cc.iriding.antcatcher.service.AntRadioServiceConnection;

public class AntService extends Service {

    private static final int CHANNEL_PROOF_DEVICE_TYPE = 0x01;
    private static final int CHANNEL_PROOF_TRANSMISSION_TYPE = 0x00;
    private static final int CHANNEL_PROOF_PERIOD = 8198; // 1 Hz
    private static final int CHANNEL_PROOF_FREQUENCY = 57;

    private Logger log = Logger.getLogger(AntService.class);

    private AntRadioServiceConnection mAntRadioServiceConnection;
    private AntProviderStateChangedReceiver mAntProviderStateChangedReceiver;

    private boolean mAntRadioServiceBound = false;

    private AntChannelProvider mAntChannelProvider;
    private boolean mAntChannelAcquired = false;
    private AntChannel mAntChannel;

    private int mDeviceNumber = -1;

    IAntServiceListener mListener = new IAntServiceListener.BaseAntChannelServiceListener();

    @Override
    public void onCreate() {
        super.onCreate();

        bindAntRadioService();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        log.warn("AntService Destroy");
        unbindAntRadioService();
        releaseChannel();
    }

    @Override
    public IBinder onBind(Intent intent) {
        log.debug("onBind");
        return new AntServiceBinder(this);
    }

    public void onAntRadioServiceConnected(com.dsi.ant.AntService antService) {
        log.debug("onAntRadioServiceConnected");
        try {
            mAntChannelProvider = antService.getChannelProvider();
            onAntProviderStateChanged(mAntChannelProvider.getNumChannelsAvailable(), mAntChannelProvider.isLegacyInterfaceInUse());
        } catch (RemoteException e) {
            log.error(e);
        }
    }

    public void onAntRadioServiceDisconnected() {
        log.info("onAntRadioServiceDisconnected");
    }

    public void onAntProviderStateChanged(int availableChannels, boolean legacyInterfaceInUse) {
        log.debug("AntProviderStateChanged,availableChannels=" + availableChannels + ",legacyInterfaceInUse=" + legacyInterfaceInUse);
        if (availableChannels > 0 || legacyInterfaceInUse) {
            unregisterReceiver(mAntProviderStateChangedReceiver);
            mAntProviderStateChangedReceiver = null;
            if (mAntChannelProvider != null) {
                if (mDeviceNumber > -1) {
                    acquireChannel();
                }
            }
        }
    }

    void onAntChannelReceiveMessage(MessageFromAntType messageType, DataMessage dataMessage) {
        log.debug("AntChannelReceiveMessage:" + messageType + ":" + dataMessage.toString());
        if (messageType == MessageFromAntType.ACKNOWLEDGED_DATA || messageType == MessageFromAntType.BROADCAST_DATA) {
            mListener.onReceiveMessage(messageType, dataMessage);
        }
    }

    void onAntChannelReceviceEvent(ChannelEventMessage eventMessage) {
        log.warn("收到Channel Event:" + eventMessage);
        switch (eventMessage.getEventCode()) {
            case TX:
                break;
            case RX_SEARCH_TIMEOUT:
                break;
            case CHANNEL_CLOSED:
                log.warn("Channel断开了:" + eventMessage.getEventCode());
                releaseChannel();
                acquireChannel();
                break;
            case CHANNEL_COLLISION:
            case RX_FAIL:
            case RX_FAIL_GO_TO_SEARCH:
            case TRANSFER_RX_FAILED:
            case TRANSFER_TX_COMPLETED:
            case TRANSFER_TX_FAILED:
            case TRANSFER_TX_START:
            case UNKNOWN:
                break;
        }
    }

    void onAntChannelDead() {
        mAntChannelAcquired = false;
        log.warn("Channel Dead");
        mListener.onDisconnected();
    }

    void start(int deviceNumber) {
        if (mDeviceNumber != deviceNumber) {
            releaseChannel();
        }
        mDeviceNumber = deviceNumber;
        acquireChannel();
    }

    /**
     * 绑定到AntRadioService,用于与Ant通信
     */
    void bindAntRadioService() {

        registerReceiver((mAntProviderStateChangedReceiver = new AntProviderStateChangedReceiver(this)),
                new IntentFilter(AntChannelProvider.ACTION_CHANNEL_PROVIDER_STATE_CHANGED));

        mAntRadioServiceBound = com.dsi.ant.AntService.bindService(this, (mAntRadioServiceConnection = new AntRadioServiceConnection(this)));

        log.debug("绑定AntRadioService:" + mAntRadioServiceBound);
    }

    void unbindAntRadioService() {
        if (mAntProviderStateChangedReceiver != null) {
            unregisterReceiver(mAntProviderStateChangedReceiver);
        }
        unbindService(mAntRadioServiceConnection);
        log.debug("解绑AntRadioService");
    }

    void acquireChannel() {
        if (mAntChannelProvider == null) {
            log.info("AntChannelProvider还未准备好");
            return;
        }
        if (mAntChannelAcquired) {
            return;
        }
        log.info("准备获取Channel,deviceNumber=" + mDeviceNumber);
        try {

            NetworkKey nk = new NetworkKey(new byte[]{(byte) 0xF7, (byte) 0xC1, 0x0D, (byte) 0xE6, 0x66, 0x5A, 0x27, 0x42, 0x52, (byte) 0xD0, 0x27, 0x4F, 0x73, 0x0E, 0x19, 0x0F});
            AntChannel channel = mAntChannelProvider.acquireChannelOnPrivateNetwork(this, nk);

            channel.setAdapterEventHandler(new AntAdapterEventHandler(this));
            channel.setChannelEventHandler(new AntChannelEventHandler(this));

            channel.assign(ChannelType.BIDIRECTIONAL_SLAVE);

            ChannelId channelId = new ChannelId(mDeviceNumber, CHANNEL_PROOF_DEVICE_TYPE, CHANNEL_PROOF_TRANSMISSION_TYPE);

            channel.setChannelId(channelId);
            channel.setRfFrequency(CHANNEL_PROOF_FREQUENCY);
            channel.setPeriod(CHANNEL_PROOF_PERIOD);

            channel.open();

            mAntChannel = channel;
            mAntChannelAcquired = true;
        } catch (RemoteException e) {
            log.error(e, e);
        } catch (UnsupportedFeatureException e) {
            log.error(e, e);
        } catch (ChannelNotAvailableException e) {
            log.error(e, e);
        } catch (AntCommandFailedException e) {
            log.error(e, e);
        }
    }

    void releaseChannel() {
        log.debug("释放Channel");
        if (mAntChannel != null) {
            mAntChannel.release();
            mAntChannel = null;
        }
        mAntChannelAcquired = false;
    }

}
