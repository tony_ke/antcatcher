package cc.iriding.antcatcher.service.ant;

import android.os.Binder;

/**
 * Created by bac on 15/12/14.
 */
public class AntServiceBinder extends Binder {
    private AntService mService;

    AntServiceBinder(AntService service) {
        mService = service;
    }

    public void setListener(IAntServiceListener listener){
        mService.mListener=listener;
    }

    public void start(int deviceNumber){
        mService.start(deviceNumber);
    }

    public void stop(){
        mService.releaseChannel();
    }


}
