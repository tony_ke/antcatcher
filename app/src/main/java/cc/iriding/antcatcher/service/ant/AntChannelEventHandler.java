package cc.iriding.antcatcher.service.ant;

import com.dsi.ant.channel.IAntChannelEventHandler;
import com.dsi.ant.message.fromant.AcknowledgedDataMessage;
import com.dsi.ant.message.fromant.BroadcastDataMessage;
import com.dsi.ant.message.fromant.ChannelEventMessage;
import com.dsi.ant.message.fromant.DataMessage;
import com.dsi.ant.message.fromant.MessageFromAntType;
import com.dsi.ant.message.ipc.AntMessageParcel;

/**
 * Created by bac on 15/12/15.
 */
public class AntChannelEventHandler implements IAntChannelEventHandler {

    private AntService mService;

    AntChannelEventHandler(AntService service){
        this.mService=service;
    }

    @Override
    public void onReceiveMessage(MessageFromAntType messageFromAntType, AntMessageParcel antParcel) {

        DataMessage dataMessage;
        switch(messageFromAntType)
        {
            // Only need to worry about receiving data
            case BROADCAST_DATA:
                // Rx Data
                dataMessage =new BroadcastDataMessage(antParcel);

                mService.onAntChannelReceiveMessage(messageFromAntType,dataMessage);
                break;
            case ACKNOWLEDGED_DATA:
                // Rx Data
                dataMessage = new AcknowledgedDataMessage(antParcel);

                mService.onAntChannelReceiveMessage(messageFromAntType,dataMessage);
                break;
            case CHANNEL_EVENT:
                ChannelEventMessage eventMessage = new ChannelEventMessage(antParcel);

                mService.onAntChannelReceviceEvent(eventMessage);

                break;
            case ANT_VERSION:
            case BURST_TRANSFER_DATA:
            case CAPABILITIES:
            case CHANNEL_ID:
            case CHANNEL_RESPONSE:
            case CHANNEL_STATUS:
            case SERIAL_NUMBER:
            case OTHER:
                // TODO More complex communication will need to handle these message types
                break;
        }
    }

    @Override
    public void onChannelDeath() {
        mService.onAntChannelDead();
    }
}
