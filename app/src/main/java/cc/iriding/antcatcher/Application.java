package cc.iriding.antcatcher;

import android.os.Environment;

import org.apache.log4j.Level;

import java.io.File;

import de.mindpipe.android.logging.log4j.LogConfigurator;

/**
 * Created by bac on 15/12/15.
 */
public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();

        LogConfigurator logConfigurator = new LogConfigurator();
        logConfigurator.setFileName(Environment.getExternalStorageDirectory() + File.separator + "cc.iriding.antcatcher"
                + File.separator + "logs" + File.separator + "log4j.log");
        logConfigurator.setRootLevel(Level.INFO);
        logConfigurator.setLevel("org.apache", Level.ERROR);
        logConfigurator.setFilePattern("%d{yyyy-MM-dd HH:mm:ss} %-5p [%c{3}]-[%L] %m%n");
        logConfigurator.setMaxFileSize(5 * 1024 * 1024);
        logConfigurator.setImmediateFlush(true);
        logConfigurator.setUseFileAppender(true);
        logConfigurator.setMaxBackupSize(1);
        logConfigurator.configure();
    }
}
