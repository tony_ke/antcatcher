package cc.iriding.antcatcher.service.antplus;

import android.os.Binder;

/**
 * Created by bac on 15/12/15.
 */
public class AntPlusServiceBinder extends
        Binder {
    private AntPlusService mService;

    AntPlusServiceBinder(AntPlusService service){
        mService=service;
    }

    public void setListener(IAntPlusServiceListener listener){
        mService.mListener=listener;
    }
    public void startBikePower(int deviceNumber,int wheelCircumference){
        mService.startBikePower(deviceNumber,wheelCircumference);
    }
    public void stopBikePower(){
        mService.stopBikePower();
    }
}
