package cc.iriding.antcatcher.service;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;

import cc.iriding.antcatcher.service.ant.AntService;

/**
 * Created by bac on 15/12/15.
 */
public class AntRadioServiceConnection implements ServiceConnection {

    private AntService mService;

    public AntRadioServiceConnection(AntService service) {
        this.mService = service;
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        com.dsi.ant.AntService antService = new com.dsi.ant.AntService(iBinder);
        mService.onAntRadioServiceConnected(antService);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        mService.onAntRadioServiceDisconnected();
    }
}
